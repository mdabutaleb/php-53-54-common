<?php
include("../../vendor/autoload.php");
use  App\Auth\Auth;

$obj1 = new Auth();
$allData = $obj1->getAllUsers();
//echo "<pre>";
//print_r($allData);
?>
<a href="create.php">Add new user</a>
<table border="1">
    <tr>
        <td>ID</td>
        <td>UserName</td>
        <td>Email</td>
        <td>Action</td>
    </tr>
    <?php
    foreach ($allData as $data) {
        ?>
        <tr>
            <td><?php echo $data['id'] ?></td>
            <td><?php echo $data['username'] ?></td>
            <td><?php echo $data['email'] ?></td>
            <td>
                <a href="show.php?id=<?php echo $data['id'] ?>">View</a>
            </td>
            <td> Edit | Delete</td>
        </tr>
    <?php } ?>
</table>