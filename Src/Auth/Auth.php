<?php
namespace App\Auth;

use PDO;

session_start();

class Auth
{
    private $username;
    private $password;
    private $email;
    private $dbuser = 'root';
    private $dbpass = '';

    public function __construct()
    {

    }

    public function setData($data = '')
    {
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        return $this;
    }

    public function getAllUsers()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=php53', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM USERS";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchAll();
        return $data;
    }

    public function store()
    {
        try {
            $pdo = new PDO('mysql:host=localhost;dbname=php53', $this->dbuser, $this->dbpass);
            $query = "INSERT INTO users(id, username, email, password, created_at)VALUES(:i, :u, :e, :p, :c)";

            $stmt = $pdo->prepare($query);
            $data = array(
                ':i' => null,
                ':u' => $this->username,
                ':e' => $this->email,
                ':p' => $this->password,
                ':c' => date('Y-m-d h:m:s'),
            );
            $status = $stmt->execute($data);
            if ($status) {
                $_SESSION['Message'] = "<h1>Successfully registerd</h1>";
                echo $_SESSION['Message'];
                header('location:create.php');

            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function show($id = '')
    {
        $pdo = new PDO('mysql:host=localhost;dbname=php53', $this->dbuser, $this->dbpass);
        $query = "SELECT * FROM `users` where id=$id";
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch();
        return $data;

    }

}